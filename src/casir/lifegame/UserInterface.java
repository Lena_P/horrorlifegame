package casir.lifegame;

import casir.lifegame.model.entity.Board;

public class UserInterface{

    public static void displayGameBoard(Board board){
        System.out.print("\033[H\033[2J");
        System.out.flush();
        String cell;
        for (int positionx=0; positionx<20; positionx++){
            cell = "";
            for (int positiony=0; positiony<20; positiony++) {
                if (board.content[positionx][positiony]){
                    cell += "#";
                } else {
                    cell += "_";
                }
            }
            System.out.println(cell);
        }
    }

    public static void displayEndMessage() {
        System.out.println("Game over");
    }

}