package casir.lifegame;

import casir.lifegame.model.entity.Board;

public class LifeGame {
	
	private Board board;
	private Board board2;

    public LifeGame(){
        this.board2 = new Board(20); //crée un premier tableau1 de 20 cases
        this.board = new Board(20); // crée un deuxième tableau2 de 20 cases
        this.board.content[17][17] = true; //Rajout des cellules vivantes dans différentes casés  du tableau 1
        this.board.content[18][17] = true;
        this.board.content[17][18] = true;
        this.board.content[19][18] = true;
        this.board.content[17][19] = true;
    }
        
    public void run() {

        while(!this.board.eq(board2)){ // tant que le tableau1 n'est pas égal au tableau2 (tant que les cellules ne sont pas toutes mortes)
            UserInterface.displayGameBoard(this.board); //affiche le tableau de jeu
            try {									//temps de pause avant la reprise du programme
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.board2 =  this.board.clone(); 
            
            checkNeighbours(board, board2);
            
            // Analyse l'environnement de chaque cellule
        }
        UserInterface.displayEndMessage();       
    }
    
    public void checkNeighbours(Board board, Board board2) {
    	for (int positionx=0; positionx<20; positionx++){
            for (int positiony=0; positiony<20; positiony++) {
                int neightbours = 0;
				for (int neighbourY = -1; neighbourY<= 1; neighbourY++){
                    for (int neighbourX = -1; neighbourX <= 1; neighbourX++){
                        if (!(neighbourY== 0 && neighbourX==0)){
                            if (positionx+neighbourX >= 0 && positionx+neighbourX < 20 && positiony + neighbourY >= 0 && positiony+neighbourY < 20){
                                boolean hasNeighbour =  board2.content[positionx+neighbourX][positiony+neighbourY];
                                if (hasNeighbour){
                                    neightbours ++;
                                }
                            }
                        }
                    }
                }
                if ( this.board2.content[positionx][positiony]){ //si la cellule est entourée de moins de deux cellules  ou de plus de trois cellules, elle meurt sinon elle vit
                    if (neightbours < 2 || neightbours > 3){
                         this.board.content[positionx][positiony] = false;
                    } else {
                         this.board.content[positionx][positiony] = true;
                    }
                } else {
                    if (neightbours == 3){ //sinon si la cellule est entourée de 3 autres cellules, elle vit, sinon elle meurt
                         this.board.content[positionx][positiony] = true;
                    } else {
                         this.board.content[positionx][positiony] = false;
                    }
                }
            }
        }
    }
}