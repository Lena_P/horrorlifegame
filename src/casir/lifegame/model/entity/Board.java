package casir.lifegame.model.entity;

public class Board {

    public boolean[][] content;

    public Board(int j){
        this.content = new boolean[j][j];
        boolean[] l;
        for (int x=0; x<j; x++){
            l = new boolean[j];
            for (int y=0; y<j; y++) {
                l[y] = false;
            }
            this.content[x] = l;
        }
    }

    public boolean eq(Board board2) {
        for (int positionx=0; positionx<this.content.length; positionx++){
            for (int positiony=0; positiony<this.content.length; positiony++) {
                if (this.content[positionx][positiony] != board2.content[positionx][positiony]){
                    return false;
                }
            }
        }
        return true;
    }

    public Board clone(){
        Board clone = new Board(this.content.length);
        for (int positionx=0; positionx<this.content.length; positionx++){
            for (int positiony=0; positiony<this.content.length; positiony++) {
                clone.content[positionx][positiony] = this.content[positionx][positiony];
            }
        }
        return clone;
    }
}